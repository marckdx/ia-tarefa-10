package exceptions;

public class ClassException extends Exception {
	
	    public ClassException(String message) {
	        super(message);
	    }
	    
	    public ClassException(){
	        super("Tentativa de defini��o de classe falhou, especifica��o das classes poss�veis n�o cont�m a que se deseja inserir");
	    }
	    

}
