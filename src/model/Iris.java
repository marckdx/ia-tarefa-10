package model;

import exceptions.ClassException;

public class Iris{

    private double sepallength;
    private double sepalwidth;
    private double petallength;
    private double petalwidth;

    private String exClass;
    
    public static String[] POS_VAL_CLASS = {"Iris-setosa", "Iris-versicolor", "Iris-virginica"};
    
    public static String IRIS_SETOSA = "Iris-setosa";
    public static String IRIS_VERSICOLOR = "Iris-versicolor";
    public static String IRIS_VIRGINICA = "Iris-virginica";
    
    public static int classCount = 3;
    
    public double exampleDist;

    /**
     *
     * @param sepallength
     * @param sepalwidth
     * @param petallength
     * @param petalwidth
     */
    public Iris(double sepallength, double sepalwidth, double petallength, double petalwidth) {
        this.sepallength = sepallength;
        this.sepalwidth = sepalwidth;
        this.petallength = petallength;
        this.petalwidth = petalwidth;
    }

    public Iris(double sepallength, double sepalwidth, double petallength, double petalwidth, String exClass) throws ClassException {
        this.sepallength = sepallength;
        this.sepalwidth = sepalwidth;
        this.petallength = petallength;
        this.petalwidth = petalwidth;
        this.setExClass(exClass);
    }

    

    public double getSepallength() {
        return sepallength;
    }

    public void setSepallength(double sepallength) {
        this.sepallength = sepallength;
    }

    public double getSepalwidth() {
        return sepalwidth;
    }

    public void setSepalwidth(double sepalwidth) {
        this.sepalwidth = sepalwidth;
    }

    public double getPetallength() {
        return petallength;
    }

    public void setPetallength(double petallength) {
        this.petallength = petallength;
    }

    public double getPetalwidth() {
        return petalwidth;
    }

    public void setPetalwidth(double petalwidth) {
        this.petalwidth = petalwidth;
    }

    public String getExClass() {
        return exClass;
    }

    public final void setExClass(String exClass) throws ClassException {
        boolean passed = false;

        for (String classValue : POS_VAL_CLASS) {
            if (exClass.equals(classValue)) {
                passed = true;
                break;
            }
        }

        if (passed) {
            this.exClass = exClass;
        }else{
            throw new ClassException();
        }
    }

    public double getExampleDist() {
        return exampleDist;
    }

    public void setExampleDist(double exampleDist) {
        this.exampleDist = exampleDist;
    }

    public String[] getPOS_VAL_CLASS() {
        return POS_VAL_CLASS;
    }

    public void setPOS_VAL_CLASS(String[] POS_VAL_CLASS) {
        this.POS_VAL_CLASS = POS_VAL_CLASS;
    }
    
   
    public String toString() {
        return "Iris{" + "sepallength=" + sepallength + ", sepalwidth=" + sepalwidth + ", petallength=" + petallength + ", petalwidth=" + petalwidth + ", class=" + exClass + ",  distance=" + exampleDist + '}';
    }

}