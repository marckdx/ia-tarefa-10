package control;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.Iris;
import classifier.KNN;
import exceptions.ClassException;

/**
*
* @author Marco Aur�lio Lima
*/
public class Main {

   public static ArrayList<Iris> base = new ArrayList<Iris>();

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) throws ClassException {

       insertIris();

       //Este exemplo pertence a classe Iris-Versicolor *.*
       Iris exemplo = new Iris(7.0,3.2,4.7,1.4);

       KNN knn = new KNN(base);
       Iris[] iris = knn.getKNN(exemplo, base, 3);

       String concat = "\n\nVizinhos: ";
       for (Iris nearest : iris) {
           concat += ("\n"+nearest.toString());
       }
       
       int[][] mode = knn.getModo(iris);
       JOptionPane.showMessageDialog(null,"A classe definida foi: "+(Iris.POS_VAL_CLASS[mode[0][0]]) + " com " + mode[0][1] +" correspondentes \n "
               + "\nExemplo: \n" + exemplo.toString() + concat);

   }

   /**
    * M�todo que insere 12 exemplos reais (abstraidos do arquivo ARFF) para cada classe
    * @throws ClassException 
    */
   private static void insertIris() throws ClassException {
       base.add(new Iris(5.1, 3.5, 1.4, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.9, 3.0, 1.4, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.7, 3.2, 1.3, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.6, 3.1, 1.5, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(5.0, 3.6, 1.4, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(5.4, 3.9, 1.7, 0.4, Iris.IRIS_SETOSA));
       base.add(new Iris(4.6, 3.4, 1.4, 0.3, Iris.IRIS_SETOSA));
       base.add(new Iris(5.0, 3.4, 1.5, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.4, 2.9, 1.4, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.9, 3.1, 1.5, 0.1, Iris.IRIS_SETOSA));
       base.add(new Iris(5.4, 3.7, 1.5, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.8, 3.4, 1.6, 0.2, Iris.IRIS_SETOSA));
       base.add(new Iris(4.8, 3.0, 1.4, 0.1, Iris.IRIS_SETOSA));

       base.add(new Iris(7.0, 3.2, 4.7, 1.4, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(6.4, 3.2, 4.5, 1.5, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(6.9, 3.1, 4.9, 1.5, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(5.5, 2.3, 4.0, 1.3, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(6.5, 2.8, 4.6, 1.5, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(5.7, 2.8, 4.5, 1.3, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(6.3, 3.3, 4.7, 1.6, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(4.9, 2.4, 3.3, 1.0, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(6.6, 2.9, 4.6, 1.3, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(5.2, 2.7, 3.9, 1.4, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(5.0, 2.0, 3.5, 1.0, Iris.IRIS_VERSICOLOR));
       base.add(new Iris(5.9, 3.0, 4.2, 1.5, Iris.IRIS_VERSICOLOR));

       base.add(new Iris(6.3, 3.3, 6.0, 2.5, Iris.IRIS_VIRGINICA));
       base.add(new Iris(5.8, 2.7, 5.1, 1.9, Iris.IRIS_VIRGINICA));
       base.add(new Iris(7.1, 3.0, 5.9, 2.1, Iris.IRIS_VIRGINICA));
       base.add(new Iris(6.3, 2.9, 5.6, 1.8, Iris.IRIS_VIRGINICA));
       base.add(new Iris(6.5, 3.0, 5.8, 2.2, Iris.IRIS_VIRGINICA));
       base.add(new Iris(7.6, 3.0, 6.6, 2.1, Iris.IRIS_VIRGINICA));
       base.add(new Iris(4.9, 2.5, 4.5, 1.7, Iris.IRIS_VIRGINICA));
       base.add(new Iris(7.3, 2.9, 6.3, 1.8, Iris.IRIS_VIRGINICA));
       base.add(new Iris(6.7, 2.5, 5.8, 1.8, Iris.IRIS_VIRGINICA));
       base.add(new Iris(7.2, 3.6, 6.1, 2.5, Iris.IRIS_VIRGINICA));
       base.add(new Iris(6.5, 3.2, 5.1, 2.0, Iris.IRIS_VIRGINICA));
       base.add(new Iris(6.4, 2.7, 5.3, 1.9, Iris.IRIS_VIRGINICA));

   }

}