package classifier;

import java.util.ArrayList;

import model.Iris;


public class KNN extends Classificador {

    private ArrayList<Iris> iris = new ArrayList<>();

    public KNN(ArrayList<Iris> iris) {
        this.iris = iris;
    }

    /**
     * M�todo que retorna quantos vizinhos mais pr�ximos existem dependendo da
     * quantidade desejada
     *
     * @param exemplo
     * @param base
     * @param quant
     * @return
     */
    public Iris[] getKNN(Iris exemplo, ArrayList<Iris> base, int quant) {
        Iris[] vizinhos = new Iris[quant];
        int cont = 0;

        for (Iris compare : base) {
            compare.exampleDist = getMinorDistance(exemplo, compare);
            //System.out.println(compare.exampleDist);
            if (cont == 0) {
                vizinhos[0] = compare;
                vizinhos[1] = compare;
                vizinhos[2] = compare;
            } else {
                if (compare.exampleDist < vizinhos[0].exampleDist) {
                    vizinhos[0] = compare;
                } else if (compare.exampleDist < vizinhos[1].exampleDist) {
                    vizinhos[1] = compare;
                } else if (compare.exampleDist < vizinhos[2].exampleDist) {
                    vizinhos[2] = compare;
                }
            }
            cont++;
        }

        return vizinhos;
    }

    /**
     * M�todo que calcula o vizinho mais pr�ximo, considerando apenas um vizinho
     *
     * @param exemplo
     * @param base
     * @return
     */
    public Iris getKNN(Iris exemplo, ArrayList<Iris> base) {
        Iris minor;
        double distMenor = 0;
        int cont = 0;
        for (Iris compare : base) {
            double thisDist = getMinorDistance(exemplo, compare);
            if (cont == 0) {
                minor = compare;
                distMenor = thisDist;
            } else {
                if (thisDist < distMenor) {
                    minor = compare;
                    distMenor = thisDist;
                }
            }
            cont++;
        }

        return null;
    }

    /**
     * M�todo que recupera o vizinho mais pr�ximo
     *
     * @param exemplo
     * @param compare
     * @return
     */
    private double getMinorDistance(Iris exemplo, Iris compare) {
        return Math.sqrt(Math.pow((exemplo.getPetallength() - compare.getPetallength()), 2)
                + Math.pow((exemplo.getPetalwidth() - compare.getPetalwidth()), 2)
                + Math.pow((exemplo.getSepallength() - compare.getSepallength()), 2)
                + Math.pow((exemplo.getSepalwidth() - compare.getSepalwidth()), 2));
    }

    public void addNewExample(Iris iris) {
        this.iris.add(iris);
    }

    public int[][] getModo(Iris[] iris) {
        int[][] mods = new int[Iris.classCount][2];
        int[][] maior = new int[1][2];

        for (int i = 0; i < Iris.classCount; i++) {
            if (iris[i].getExClass().equals(Iris.IRIS_SETOSA)) {
                mods[0][1]++;

                if (mods[0][1] > maior[0][1]) {
                    maior[0][0] = 0;
                    maior[0][1] = mods[0][1];
                }
            }
            if (iris[i].getExClass().equals(Iris.IRIS_VERSICOLOR)) {
                mods[1][1]++;
                if (mods[1][1] > maior[0][1]) {
                    maior[0][0] = 1;
                    maior[0][1] = mods[1][1];
                }
            }
            if (iris[i].getExClass().equals(Iris.IRIS_VIRGINICA)) {
                mods[2][1]++;
                if (mods[2][1] > maior[0][1]) {
                    maior[0][0] = 2;
                    maior[0][1] = mods[2][1];
                }
            }
        }
        return maior;
    }

    public String getClassModa(Iris[] iris) {
        int[][] moda = getModo(iris);
        return Iris.POS_VAL_CLASS[moda[0][0]];
    }

}

